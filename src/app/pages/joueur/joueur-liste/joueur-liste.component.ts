import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Joueur } from 'src/app/interfaces/joueur';
import { JoueurService } from 'src/app/services/joueur.service';

@Component({
  selector: 'app-joueur-liste',
  templateUrl: './joueur-liste.component.html',
  styleUrls: ['./joueur-liste.component.scss']
})
export class JoueurListeComponent implements OnInit{

  // VARIABLE DE PAGINATION
  paginationCurrentPage: number = 1; // Page actuelle
  paginationItemsPerPage: number = 10; // Nombre d'éléments par page
  paginatedItems: Joueur[] = []; // Joueurs paginés pour la vue actuelle
  maxPagesToShow: number = 15; // Nombre maximum de pages à afficher

  joueurs: Joueur[] = [];
  joueur?: Joueur | null = null;

  constructor(
    private http: HttpClient,
    private router: Router,
    private joueurService: JoueurService,
    private route: ActivatedRoute
    ){}

  ngOnInit(): void {
    this.joueurService.getJoueurs().subscribe(
      (joueurs) => {
        this.joueurs = joueurs;
        this.paginateItems(); // Paginer les joueurs après les avoir reçus
      },
      (error) => {
        console.error('Erreur lors de la récupération des joueurs:', error);
      }
    );
  }
  /*---------------- Fonctions pour la pagination  -----------------*/
  paginateItems(): void {
    const startIndex = (this.paginationCurrentPage - 1) * this.paginationItemsPerPage;
    const endIndex = startIndex + this.paginationItemsPerPage;
    if (this.joueurs) {
      console.log('Start Index:', startIndex);
      console.log('End Index:', endIndex);
      this.paginatedItems = this.joueurs.slice(startIndex, endIndex);
    }
  }
  onPageChanged(pageNumber: number): void {
    this.paginationCurrentPage = pageNumber;
    this.paginateItems(); // Rafraîchir les joueurs paginés pour la nouvelle page sélectionnée
  }



/*---------------- Fonctions de tri -----------------*/
triParNom(): void {
  this.joueurs?.sort((a, b) => a.nom.localeCompare(b.nom));
}
triParTaille(): void {
  this.joueurs?.sort((a, b) => a.taille - b.taille);
}

triParAge(): void {
  this.joueurs?.sort((a, b) => a.age - b.age);
}

triParNationalite(): void {
  this.joueurs?.sort((a, b) => a.nationalite.localeCompare(b.nationalite));
}

triParPosition(): void {
  this.joueurs?.sort((a, b) => a.position.localeCompare(b.position));
}

/*---------------- Fonctions de navigation -----------------*/
  goToJoueur(joueur: Joueur){
    this.router.navigate(["/joueur/", joueur.id]);
}

  goToEdit(joueur: Joueur){
    this.router.navigate(["/joueur/edit/", joueur.id])
  }

  effacerJoueur(joueur: Joueur){
    console.log("joueur :", joueur.id)
    if(confirm("Voulez-vous supprimer ce joueur ?")) {
      this.joueurService.deleteJoueur(joueur.id).subscribe(() => {  
        this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
          this.router.navigate(['/joueurliste']);
        }); 
      }); 
    }       
  }
  
    
    




      /*---------------- Affichage des photos des joueurs en aléatoire -----------------*/
  choixPhoto(): string {
    let numPortrait = Math.floor(Math.random() * 14) + 1;
    return `../assets/images/photoJoueur/portrait${numPortrait}.jpeg`;
  }



  /*---------------- Affichage des drapeaux des nationalités des joueurs -----------------*/
  getDrapeau(joueur?: Joueur): string {
    if (joueur?.nationalite === "Écossaise") {
        return "../assets/images/drapeaux/ecosse.png";
    }
    else if (joueur && joueur.nationalite === "Française") {
        return "../assets/images/drapeaux/france.png";
    }
    else if (joueur && joueur.nationalite === "Anglaise") {
        return "../assets/images/drapeaux/angleterre.png";
    }
    else if (joueur && joueur.nationalite === "Irlandaise") {
        return "../assets/images/drapeaux/irlande.png";
    }
    else if (joueur && joueur.nationalite === "Italienne") {
        return "../assets/images/drapeaux/italie.png";
    }
    else if (joueur && joueur.nationalite === "Allemande") {
        return "../assets/images/drapeaux/allemagne.png";
    }
    else if (joueur && joueur.nationalite === "Américaine") {
      return "../assets/images/drapeaux/usa.png";
  }
    return "../assets/images/drapeaux/olympique.png";
}
/*---------------- Affichage des logos des equipes des joueurs -----------------*/
getEquipeLogo(joueur : Joueur): string {
  if (joueur.equipe.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
  }
  else if (joueur!.equipe.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if (joueur!.equipe.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
  else if (joueur!.equipe.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
  else if (joueur!.equipe.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
  else if (joueur!.equipe.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
  }
  else if (joueur!.equipe.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/dundee.png";
  }
  else if (joueur!.equipe.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (joueur!.equipe.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (joueur!.equipe.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if (joueur!.equipe.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
  else if (joueur!.equipe.nomCourt == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}





}
