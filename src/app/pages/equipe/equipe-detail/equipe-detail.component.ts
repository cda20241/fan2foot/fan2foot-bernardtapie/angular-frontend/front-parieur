import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Equipe, EquipeJoueurs } from 'src/app/interfaces/equipe';
import { Joueur } from 'src/app/interfaces/joueur';
import { Match, MatchDTO } from 'src/app/interfaces/match';
import { EquipeService } from 'src/app/services/equipe.service';
import { RencontreService } from 'src/app/services/rencontre.service';

@Component({
  selector: 'app-equipe-detail',
  templateUrl: './equipe-detail.component.html',
  styleUrls: ['./equipe-detail.component.scss']
})
export class EquipeDetailComponent implements OnInit{
equipeJoueurs!: EquipeJoueurs;
match!: Match;
matchListe: MatchDTO[] = [];
allMatch: Match[]=[];
eqDomicile! : EquipeJoueurs;
matchPasses: MatchDTO[] = []
joueur?: Joueur;
urlId!: number;


constructor(
  private equipeService: EquipeService,
  private rencontreService : RencontreService,
  private route: ActivatedRoute,
    private router : Router)
{}

ngOnInit(): void {
    this.equipeService.getEquipe(+this.route.snapshot.paramMap.get(`id`)!)
    .subscribe(equipe => {
      this.equipeJoueurs = equipe;
      this.equipeJoueurs.joueurs.sort((a, b) => a.numero - b.numero);
      /**Création de deux liste MatchListe pour les matchs à venir et matchPasses pour les matchs qui ont déjà eu lieu */
      for (let match of this.equipeJoueurs.rencontres) {
        const date = new Date();
        const dateMatch = new Date(match.date);
        if (dateMatch.getTime() >= date.getTime()){
          this.matchListe.push(match);
        } else {
          this.matchPasses.push(match);
        }
      }
    }); 
  }

goToThisPari(match: MatchDTO){
    console.log("id du Pari : ", match.id)
    this.router.navigate(["pari_detail/", match.id])
}



  goToJoueur(joueur: Joueur){
    this.router.navigate(["/joueur/", joueur.id]);
}

goToEquipeDomicile(match: MatchDTO){
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    const id = match.eqDomicile.id
    console.log(id)
    this.router.navigate(["/equipe/"+ id]);
}

goToEquipeExterieur(match: MatchDTO){
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    console.log(match.eqExterieur.id)
     this.router.navigate(["/equipe/", match.eqExterieur.id]);
}




// /*---------------- Affichage des logos de l'équipe-----------------*/
  getEquipeLogo(equipeJoueurs : EquipeJoueurs): string {
   if ( equipeJoueurs.nomLong=== "Glasgow Ranger") {
       return "../assets/images/equipeLogo/glasgowRangers.png";
}
   else if (equipeJoueurs.nomLong == "Celtic Glasgow") {
       return "../assets/images/equipeLogo/celtic.png";
   }
   else if (equipeJoueurs.nomLong == "Heart of Midlothian FC") {
       return "../assets/images/equipeLogo/heart.png";
   }
  else if (equipeJoueurs.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
   else if (equipeJoueurs.nomLong == "St Mirren FC") {
       return "../assets/images/equipeLogo/saintMirren.png";
   }
  else if (equipeJoueurs.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
   }
   else if (equipeJoueurs.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/dundee.png";
}
  else if (equipeJoueurs.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (equipeJoueurs.nomLong == "Aberdeen FC") {
       return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (equipeJoueurs.nomLong == "St Johnstone FC") {
       return "../assets/images/equipeLogo/stJohnstone.png";
   }
   else if (equipeJoueurs.nomLong == "Ross County") {
       return "../assets/images/equipeLogo/rossCounty.png";
   }
  else if (equipeJoueurs.nomCourt == "Livingston FC") {
       return "../assets/images/equipeLogo/livingstone.png";
   }
 return "../assets/images/equipeLogo/logo-13.png";
}

getLogoDomicile(match: MatchDTO): string {
  console.log("equipe Domicile " + match.eqExterieur.nomLong + "equipe Exterieur" + match.eqExterieur.nomLong) 
  if ( match.eqDomicile.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
}
  else if ( match.eqDomicile.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if ( match.eqDomicile.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
 else if ( match.eqDomicile.nomLong == "Kilmarnock FC") {
     return "../assets/images/equipeLogo/kilmarnock.png";
 }
  else if ( match.eqDomicile.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
 else if ( match.eqDomicile.nomLong == "Hibernian FC") {
     return "../assets/images/equipeLogo/hibernian.png";
  }
  else if ( match.eqDomicile.nomLong == "Dundee FC") {
     return "../assets/images/equipeLogo/dundee.png";
}
 else if ( match.eqDomicile.nomLong == "Motherwell FC") {
     return "../assets/images/equipeLogo/motherwell.png";
 }
 else if ( match.eqDomicile.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
 }
 else if ( match.eqDomicile.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if ( match.eqDomicile.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
 else if ( match.eqDomicile.nomLong == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}


getLogoExterieur(match: MatchDTO): string {

  if (match.eqExterieur.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
}
  else if ( match.eqExterieur.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if ( match.eqExterieur.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
 else if ( match.eqExterieur.nomLong == "Kilmarnock FC" ) {
     return "../assets/images/equipeLogo/kilmarnock.png";
 }
  else if ( match.eqExterieur.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
 else if ( match.eqExterieur.nomLong == "Hibernian FC") {
     return "../assets/images/equipeLogo/hibernian.png";
  }
  else if ( match.eqExterieur.nomLong == "Dundee FC") {
     return "../assets/images/equipeLogo/dundee.png";
}
 else if ( match.eqExterieur.nomLong == "Motherwell FC") {
     return "../assets/images/equipeLogo/motherwell.png";
 }
 else if ( match.eqExterieur.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
 }
 else if ( match.eqExterieur.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if ( match.eqExterieur.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
 else if ( match.eqExterieur.nomLong == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}
}


 export interface StatsJoueurs{
    nom: string,
    buts: number, 
    passes: number
 }