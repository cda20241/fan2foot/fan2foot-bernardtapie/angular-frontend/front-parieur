import { Component, OnInit } from '@angular/core';
import { Equipe } from '../../../interfaces/equipe';
import { EquipeService } from '../../../services/equipe.service';
import { RencontreService } from 'src/app/services/rencontre.service';
import { Match } from 'src/app/interfaces/match';
import { Router } from '@angular/router';


@Component({
  selector: 'app-liste-equipes',
  templateUrl: './liste-equipes.component.html',
  styleUrls: ['./liste-equipes.component.scss']
})
export class ListeEquipesComponent implements OnInit {

  equipeList: Equipe[]=[];
  equipe: Equipe| null = null;
  matchList: { [equipe: string]: Match[] } = {};
  match: Match | null=null;
  matchAffiche: Match[]=[];

  constructor(
    private equipeService: EquipeService,
    private rencontreService: RencontreService,
    private router: Router,){}

    ngOnInit(): void {
      this.getEquipeList();
    }

    getEquipeList(): void {
      this.equipeService.getEquipeList().subscribe(
        (equipeList) => {
          this.equipeList = equipeList;
          for (let equipe of this.equipeList) {
            this.getMatchList(equipe.nomCourt);
          }
        },
        (error) => {
          console.error('Erreur lors de la sélection des equipes:', error);
        }
      );
    }

    getMatchList(equipe: string): void {
      this.rencontreService.getMatchPasses().subscribe(
        (matchList) => {
          // Filtrer les matchs pour n'inclure que ceux de l'équipe spécifiée
          const matchsDeLEquipe = matchList.filter(match => match.eqDomicile.nomCourt === equipe || match.eqExterieur.nomCourt === equipe);

          // Trier les matchs par date et prendre les 3 derniers
          matchsDeLEquipe.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
          this.matchList[equipe] = matchsDeLEquipe.slice(0, 3);
        },
        (error) => {
          console.error('Erreur lors de la récupération de la liste des matchs:', error);
        }
      );
    }





    goToEquipe(equipe: Equipe) {
      this.router.navigate(["/equipe/", equipe.id])
  }



  /*---------------- Affichage des logos des equipes des joueurs -----------------*/
getEquipeLogo(equipe : Equipe): string {
  if (equipe.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
  }
  else if (equipe.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if (equipe.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
  else if (equipe.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
  else if (equipe.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
  else if (equipe.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
  }
  else if (equipe.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/dundee.png";
  }
  else if (equipe.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (equipe.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (equipe.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if (equipe.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
  else if (equipe.nomCourt == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}
}


