import {Component, OnInit} from '@angular/core';
import {Match, MatchsParDate} from "../../../interfaces/match";
import {RencontreService} from "../../../services/rencontre.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-pari-liste',
  templateUrl: './pari-liste.component.html',
  styleUrls: ['./pari-liste.component.scss'],
  providers: [DatePipe]
})
export class PariListeComponent implements OnInit{
  matchs?:Match[];
  matchsParDate:MatchsParDate[]=[];
  groupesParDate: Map<string, Match[]> = new Map();

  // VARIABLE DE PAGINATION
  paginationCurrentPage: number = 1; // Page actuelle
  paginationItemsPerPage: number = 3; // Nombre d'éléments par page
  paginatedItems: MatchsParDate[]=[]; // Joueurs paginés pour la vue actuelle
  maxPagesToShow: number = 15; // Nombre maximum de pages à afficher

  constructor(private rencontreService:RencontreService, private datePipe:DatePipe) {}

  ngOnInit() {
    this.rencontreService.getListeMatchs().subscribe(
      (res) => {
        this.matchs = res;
        this.groupesParDate = this.matchs.reduce((groupes, match) => {
          const date = match.date;
          const groupe = groupes.get(date);
          if (groupe) {
            groupe.push(match);
          } else {
            groupes.set(date, [match]);
          }
          return groupes;
        }, new Map());
        for (const [date, matches] of this.groupesParDate) {
          // Créer un nouvel objet `GroupesParDate` pour chaque élément
          const groupeParDate: MatchsParDate = {
            date,
            matches,
          };

          // Ajouter l'objet à la liste
          this.matchsParDate.push(groupeParDate);
        }
        this.paginateItems()
      }
  )
  }

  // méthode pour extraire le nom du jour
  extraireNomJour(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'EEEE', 'fr');
  }

  // méthode pour extraire le jour et le mois
  extraireJourEtMois(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'dd MMMM', 'fr');
  }
  //Methodes de PAGINATION
  paginateItems(): void {
    const startIndex = (this.paginationCurrentPage - 1) * this.paginationItemsPerPage;
    const endIndex = startIndex + this.paginationItemsPerPage;
    if (this.matchsParDate) {
      console.log('Start Index:', startIndex);
      console.log('End Index:', endIndex);
      this.paginatedItems = this.matchsParDate.slice(startIndex, endIndex);
    }
  }
  onPageChanged(pageNumber: number): void {
    this.paginationCurrentPage = pageNumber;
    this.paginateItems(); // Rafraîchir les joueurs paginés pour la nouvelle page sélectionnée
  }


}
