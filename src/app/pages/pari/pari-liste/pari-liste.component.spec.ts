import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PariListeComponent } from './pari-liste.component';

describe('PariListeComponent', () => {
  let component: PariListeComponent;
  let fixture: ComponentFixture<PariListeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PariListeComponent]
    });
    fixture = TestBed.createComponent(PariListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
