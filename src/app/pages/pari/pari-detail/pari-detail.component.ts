import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MatchDetails } from 'src/app/interfaces/matchDetails';
import { RencontreService } from 'src/app/services/rencontre.service';

@Component({
  selector: 'app-pari-detail',
  templateUrl: './pari-detail.component.html',
  styleUrls: ['./pari-detail.component.scss']
})
export class PariDetailComponent {
  matchId: number | null = null;
  matchDetails$: Observable<MatchDetails> | any;
  match?:MatchDetails;
  statutMatch?:string;

  constructor(private route: ActivatedRoute, private rencontreService: RencontreService) {}

  ngOnInit(): void {
    this.getMatchIdFromUrl(); // Appel de la methode pour récupéré l'id du match dans l'url
  }

  getMatchIdFromUrl(): void {

    const urlId = this.route.snapshot.paramMap.get('id');
    if (urlId !== null) {
      this.matchId = +urlId;
      this.getMatchDetails(); // Appel de la methode pour récupéré les details du match
    } else {
      console.error("Id du match introuvable dans l'url")
    }
  }


  getMatchDetails(): void {
    if (this.matchId !== null) {
      console.log(this.matchId)
      this.matchDetails$ = this.rencontreService.getMatchDetails(this.matchId);
      console.log(this.matchDetails$)
      this.rencontreService.getMatchDetails(this.matchId).subscribe((res) => {
        this.match = res;
        this.calculerStatutMatch(this.match!.date, this.match!.heure)
      })
    }
  }

  // méthode pour calculer le statut du match en fonction de l'heure et la date
  calculerStatutMatch(dateString: string, timeString: string): string {
    const dateMatch = new Date(dateString);
    const [heure, minute] = timeString.split(':');
    dateMatch.setHours(parseInt(heure, 10), parseInt(minute, 10), 0, 0);
    console.log(dateMatch)
    const maintenant = new Date();
    console.log(maintenant)
    const differenceMinutes = Math.round((dateMatch.getTime() - maintenant.getTime()) / (1000 * 60));
    console.log(differenceMinutes)
    if (differenceMinutes > 90) {
      return this.statutMatch = 'À venir';
    } else if (differenceMinutes <= 0 && differenceMinutes >= -90) {
      return this.statutMatch = 'En cours';
    } else {
      return this.statutMatch = 'Terminé';
    }
  }

}



