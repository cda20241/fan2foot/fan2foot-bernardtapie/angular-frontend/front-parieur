import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PariDetailComponent } from './pari-detail.component';

describe('PariDetailComponent', () => {
  let component: PariDetailComponent;
  let fixture: ComponentFixture<PariDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PariDetailComponent]
    });
    fixture = TestBed.createComponent(PariDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
