import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Joueur } from 'src/app/interfaces/joueur';
import { Portemonnaie } from 'src/app/interfaces/portemonnaie';
import { Utilisateur } from 'src/app/interfaces/utilisateur';
import { JoueurService } from 'src/app/services/joueur.service';
import { PortemonnaieService } from 'src/app/services/portemonnaie.service';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { MatDialog } from '@angular/material/dialog';
import { UtilisateurEditComponent } from 'src/app/pop-ups/utilisateur-edit/utilisateur-edit.component';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-utilisateur-detail',
  templateUrl: './utilisateur-detail.component.html',
  styleUrls: ['./utilisateur-detail.component.scss']
})
export class UtilisateurDetailComponent implements OnInit {
  utilisateur!: Utilisateur;
  portemonnaie!: Portemonnaie ;
  joueur! : Joueur;
  favorisUtilisateurs: Joueur[] = [];

  constructor(
    private utilisateurService : UtilisateurService,
    private portemonnaieService : PortemonnaieService,
    private joueurService: JoueurService,
    private route: ActivatedRoute,
    private router : Router,
    public dialog: MatDialog,
    private modalService : NgbModal
    ){}
  
    ngOnInit(): void {
      this.chargerlesdonnees();
    }

    chargerlesdonnees():void {
      this.utilisateurService.getUtilisateur(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe(utilisateur => {
          this.utilisateur = utilisateur;
          this.creerlistefavoris();
        });
      this.portemonnaieService.getPorteMonnaie(+this.route.snapshot.paramMap.get(`id`)!)
        .subscribe(portemonnaie => this.portemonnaie = portemonnaie);
    }
    
    creerlistefavoris(): void {
      this.utilisateur.favoris.forEach(favori => {
        this.getJoueurDetails(favori.idJoueur);
      });
    }
    
    getJoueurDetails(id: number): void {
      this.joueurService.getJoueur(id).subscribe(joueur => {
        this.favorisUtilisateurs.push(joueur);
      });
    }
    

choixPhoto(): string {
  let numPortrait = Math.floor(Math.random() * 14) + 1;
  return `../assets/images/photoJoueur/portrait${numPortrait}.jpeg`;
}

goToJoueur(joueur: Joueur){
  this.router.navigate(["/joueur/", joueur.id]);
}


goToEdit(utilisateur : Utilisateur) {
  const modalRef = this.modalService.open(UtilisateurEditComponent, { size : "xl" });
  modalRef.componentInstance.utilisateur = utilisateur;
  console.log(utilisateur.role)
  }


retirerFavori(joueur: Joueur): void {
  console.log("utilisateur id : " + this.utilisateur.id + " joueurid : " + joueur.id)
  this.utilisateurService.supprimerFavori(this.utilisateur.id, joueur.id)
    .subscribe(() => {
      this.favorisUtilisateurs = this.favorisUtilisateurs.filter(joueur => joueur !== joueur);
      this.chargerlesdonnees();
      this.router.navigate(['/utilisateur/', this.utilisateur.id]); // Ajoutez cette ligne
    });
}


/*---------------- Affichage des logos des equipes des joueurs -----------------*/
getEquipeLogo(joueur : Joueur): string {
  if (joueur.equipe.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
  }
  else if (joueur!.equipe.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
  }
  else if (joueur!.equipe.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
  }
  else if (joueur!.equipe.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
  }
  else if (joueur!.equipe.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
  }
  else if (joueur!.equipe.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
  }
  else if (joueur!.equipe.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/dundee.png";
  }
  else if (joueur!.equipe.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
  }
  else if (joueur!.equipe.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
  }
  else if (joueur!.equipe.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
  }
  else if (joueur!.equipe.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
  }
  else if (joueur!.equipe.nomCourt == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
  }
return "../assets/images/equipeLogo/logo-13.png";
}
  }


