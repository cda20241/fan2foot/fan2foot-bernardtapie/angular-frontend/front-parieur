import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import { MatchInfoComponent } from './components/match-info/match-info.component';
import { MatchListeComponent } from './components/match-liste/match-liste.component';
import { JoueursListeComponent } from './components/joueurs-liste/joueurs-liste.component';
import { JoueurComponent } from './components/joueur/joueur.component';
import { InformationsMatchComponent } from './components/informations-match/informations-match.component';
import { EquipesListeComponent } from './components/equipes-liste/equipes-liste.component';
import { EquipeVisiteuseComponent } from './components/equipe-visiteuse/equipe-visiteuse.component';
import { EquipeDomicileComponent } from './components/equipe-domicile/equipe-domicile.component';
import { ClassementComponent } from './components/classement/classement.component';
import { JoueurListeComponent } from './pages/joueur/joueur-liste/joueur-liste.component';
import { JoueurDetailComponent } from './pages/joueur/joueur-detail/joueur-detail.component';
import { ListeEquipesComponent } from './pages/equipe/liste-equipes/liste-equipes.component';
import { EquipeDetailComponent } from './pages/equipe/equipe-detail/equipe-detail.component';
import { RegisterComponent } from './pop-ups/register/register.component';
import { ValiderPariComponent } from './pop-ups/valider-pari/valider-pari.component';
import { ErreurPaiementComponent } from './pop-ups/erreur-paiement/erreur-paiement.component';
import { PariDetailComponent } from './pages/pari/pari-detail/pari-detail.component';
import { PariListeComponent } from './pages/pari/pari-liste/pari-liste.component';
import { PariComponent } from './components/pari/pari.component';
import { ParrainageComponent } from './components/parrainage/parrainage.component';
import { UserPariComponent } from './components/user-pari/user-pari.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { RencontreDetailComponent } from './pages/rencontre/rencontre-detail/rencontre-detail.component';
import { UserPariFiniComponent } from './components/user-pari-fini/user-pari-fini.component';
import { UtilisateurDetailComponent } from './pages/utilisateur/utilisateur-detail/utilisateur-detail.component';
import { UtilisateurEditComponent } from './pop-ups/utilisateur-edit/utilisateur-edit.component';

import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AppComponent,
    PaginationComponent,
    ProfilePreviewComponent,
    MatchInfoComponent,
    MatchListeComponent,
    JoueursListeComponent,
    JoueurComponent,
    InformationsMatchComponent,
    EquipesListeComponent,
    EquipeVisiteuseComponent,
    EquipeDomicileComponent,
    ClassementComponent,
    JoueurListeComponent,
    JoueurDetailComponent,
    ListeEquipesComponent,
    EquipeDetailComponent,
    RegisterComponent,
    ValiderPariComponent,
    ErreurPaiementComponent,
    PariDetailComponent,
    PariListeComponent,
    PariComponent,
    ParrainageComponent,
    PariComponent,
    UserPariComponent,
    RencontreDetailComponent,
    UserPariFiniComponent,
    UtilisateurDetailComponent,
    UtilisateurEditComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,

  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' } // Définit la locale par défaut en français
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    registerLocaleData(localeFr, 'fr'); // Enregistre la configuration de la locale française
  }
}
