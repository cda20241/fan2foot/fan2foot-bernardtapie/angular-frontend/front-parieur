import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environement } from 'src/environments/environment';
import { Portemonnaie } from '../interfaces/portemonnaie';

@Injectable({
  providedIn: 'root'
})
export class PortemonnaieService {
  apiUrl=environement.apiPari;
  
  constructor(
    private http: HttpClient,
  ) { }


  getPorteMonnaie(id: number): Observable<Portemonnaie>{
    return this.http.get<Portemonnaie>(this.apiUrl + '/portemonnaie/'+id );
  }
}
