import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environement } from 'src/environments/environment';
import { Match } from '../interfaces/match';
import { MatchDetails } from 'src/app/interfaces/matchDetails';

@Injectable({
  providedIn: 'root'
})
export class RencontreService {
  apiFoot=environement.apiFoot+'/rencontre'
    httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(
      private http: HttpClient,
    ) { }


  getListeMatchs():Observable<Match[]> {
    return this.http.get<Match[]>(this.apiFoot + '/liste')
  }

  getMatchDetails(matchId: number): Observable<MatchDetails> {
    return this.http.get<any>(this.apiFoot + "/" + matchId);
  }

  getMatchAVenir():Observable<Match[]>{
    return this.http.get<Match[]>(this.apiFoot + '/avenir');
  }

  getMatchPasses():Observable<Match[]>{
    return this.http.get<Match[]>(this.apiFoot + '/passes')
  }
}
