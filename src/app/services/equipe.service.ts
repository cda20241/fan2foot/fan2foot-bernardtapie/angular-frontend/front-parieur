import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Equipe, EquipeJoueurs } from '../interfaces/equipe';
import { HttpClient } from '@angular/common/http';
import { environement } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EquipeService {

  apiFoot=environement.apiFoot+'/equipe'

  constructor(
    private http: HttpClient,
  ) { }


  getEquipeList(): Observable<Equipe[]> {
    return this.http.get<Equipe[]>(this.apiFoot + '/equipes_sans_joueurs' );
  }

  creerEquipe(equipe: Equipe): Observable<any> {
    return this.http.post(this.apiFoot+'/creer', equipe, {responseType: 'text'});
  }

  updateEquipe(id: number, equipe: Equipe): Observable<string>{
    return this.http.put<string>(this.apiFoot + '/modifier/' + id, equipe)
  }

  supprimerEquipe(id:number) : Observable<string>{
    return this.http.delete<string>(this.apiFoot + '/supprimer/' + id);
  }

  getEquipe(id: number): Observable<EquipeJoueurs>{
    return this.http.get<EquipeJoueurs>(this.apiFoot + '/'+id)
  }
}
