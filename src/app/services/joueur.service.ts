import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { Joueur } from '../interfaces/joueur';
import { Observable } from 'rxjs';
import { environement } from '../../environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class JoueurService {
  apiFoot=environement.apiFoot+'/joueur'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(
    private http: HttpClient,
  ) { }

  getJoueurs(): Observable<Joueur[]> {
    return this.http.get<Joueur[]>(this.apiFoot + '/liste' );
  }

  getJoueur(id: number): Observable<Joueur>{
    return this.http.get<Joueur>(this.apiFoot + '/'+id );
  }

  deleteJoueur(id: number): Observable<string>{
    return this.http.delete<string>(this.apiFoot + '/supprimer/'+id );
  }

  updateJoueur(id : number, joueur: Joueur): Observable<string>{
    console.log("joueur:" + joueur.nom)
    return this.http.put<string>(this.apiFoot + '/modifier/'+id, joueur);
  }

  createJoueur(joueur: Joueur): Observable<Joueur>{
    return this.http.post<Joueur>(this.apiFoot + '/creer', joueur);
  }
  
}
