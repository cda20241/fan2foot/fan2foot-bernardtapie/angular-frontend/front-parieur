import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environement } from 'src/environments/environment';
import { Utilisateur, UtilisateurIdRole } from '../interfaces/utilisateur';

@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  
  apiUrl=environement.apiUtilisateur+'/utilisateur'
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  
  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Fonction qui va chercher dans l'API Utilisateur la liste des Utilisateurs avec leur role
   * @returns 
   */
  getUtilisateursAvecRole(): Observable<Utilisateur[]> {
    return this.http.get<Utilisateur[]>(this.apiUrl + '/avecrole/liste' );
  }

  getUtilisateur(id: number): Observable<Utilisateur>{
    return this.http.get<Utilisateur>(this.apiUrl + '/'+id );
  }

  deleteUtilisateur(id: number): Observable<string>{
    return this.http.delete<string>(this.apiUrl + '/supprimer/'+id );
  }

  updateUtilisateur(id : number, utilisateur: Utilisateur): Observable<string>{
    return this.http.put<string>(this.apiUrl + '/modifier/'+id, utilisateur);
  }

  supprimerFavori(idUtilisateur: number, idJoueur: number): Observable<any> {
    return this.http.delete(this.apiUrl + `/supprimer/favori/${idUtilisateur}`, { body: { idJoueur: idJoueur } });
  }
}