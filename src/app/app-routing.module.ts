import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PariListeComponent } from './pages/pari/pari-liste/pari-liste.component';
import { EquipeDetailComponent } from './pages/equipe/equipe-detail/equipe-detail.component';
import { JoueurListeComponent } from './pages/joueur/joueur-liste/joueur-liste.component';
import { ListeEquipesComponent } from './pages/equipe/liste-equipes/liste-equipes.component';
import {PariDetailComponent} from "./pages/pari/pari-detail/pari-detail.component";
import { JoueurDetailComponent } from './pages/joueur/joueur-detail/joueur-detail.component';
import { UtilisateurDetailComponent } from './pages/utilisateur/utilisateur-detail/utilisateur-detail.component';

const routes: Routes = [
  {path:"", redirectTo:"accueil", pathMatch:"full"},
  {path:"accueil", component:PariListeComponent},
  {path:"equipe/:id", component:EquipeDetailComponent},
  {path:"joueur/liste", component:JoueurListeComponent},
  {path:"equipelist", component:ListeEquipesComponent},
  {path:"pari_detail/:id", component:PariDetailComponent},
  {path:"equipelist", component:ListeEquipesComponent},
  {path:"joueur/:id", component:JoueurDetailComponent },
  { path :'utilisateur/:id', component: UtilisateurDetailComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
