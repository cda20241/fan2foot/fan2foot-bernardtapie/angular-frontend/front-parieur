import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ValiderPariComponent } from './valider-pari.component';

describe('ValiderPariComponent', () => {
  let component: ValiderPariComponent;
  let fixture: ComponentFixture<ValiderPariComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ValiderPariComponent]
    });
    fixture = TestBed.createComponent(ValiderPariComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
