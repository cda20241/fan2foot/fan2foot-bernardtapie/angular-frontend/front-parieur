import { Component, Input, OnInit } from '@angular/core';
import { Utilisateur, UtilisateurIdRole } from 'src/app/interfaces/utilisateur';
import { UtilisateurService } from 'src/app/services/utilisateur.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { NgbModalWindow } from '@ng-bootstrap/ng-bootstrap/modal/modal-window';
import { Router } from '@angular/router';

@Component({
  selector: 'app-utilisateur-edit',
  templateUrl: './utilisateur-edit.component.html',
  styleUrls: ['./utilisateur-edit.component.scss']
})
export class UtilisateurEditComponent implements OnInit {
  @Input() utilisateur!: Utilisateur;
  formUtilisateur: FormGroup = new FormGroup({
    nom: new FormControl("", [Validators.required, noSpecialChars()]),
    prenom: new FormControl("", [Validators.required, noSpecialChars()]),
    email: new FormControl(""),
    date_naissance: new FormControl(""),
  });

  constructor(
    private utilisateurService: UtilisateurService,
    public activeModal: NgbActiveModal,
    private router : Router
  ) {}

  ngOnInit() {
    this.initForm();
  }

  valider() {
    if (this.formUtilisateur.valid) {
      const utilisateurToUpdate: UtilisateurIdRole = {
        id: this.utilisateur.id,
        nom: this.formUtilisateur.get("nom")!.value as string,
        prenom: this.formUtilisateur.get("prenom")!.value as string,
        email: this.formUtilisateur.get("email")!.value as string,
        date_naissance: this.formUtilisateur.get("date_naissance")!.value as Date,
        role: this.utilisateur.role,
        mdp: this.utilisateur.mdp,
        favoris: this.utilisateur.favoris,
        avatar: this.utilisateur.avatar
      };
      console.log("Utilisateur Validation : " + this.utilisateur.prenom)
      this.utilisateurService.updateUtilisateur(utilisateurToUpdate.id, utilisateurToUpdate).subscribe(() => {
      });
    } 
    this.activeModal.close('mot');
  }

  annuler(){
    this.activeModal.close('autre mot');
  }

  initForm() {
    if (this.utilisateur) {
      this.formUtilisateur.patchValue({
        id: this.utilisateur.id,
        nom: this.utilisateur.nom,
        prenom: this.utilisateur.prenom,
        email: this.utilisateur.email,
        date_naissance: this.utilisateur.date_naissance,
        role: this.utilisateur.role,
        favoris: this.utilisateur.favoris,
        mdp: this.utilisateur.mdp,
        avatar: this.utilisateur.avatar
      });
    }
  }

  ImagePrincipale(numeroImage: number) {
    this.utilisateur.avatar = numeroImage;
    console.log('Nouvel avatar sélectionné :', numeroImage);
  }
}

function noSpecialChars(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = /[^a-zA-Z]/.test(control.value);
    return forbidden ? { noSpecialChars: { value: control.value } } : null;
  };
}
