import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErreurPaiementComponent } from './erreur-paiement.component';

describe('ErreurPaiementComponent', () => {
  let component: ErreurPaiementComponent;
  let fixture: ComponentFixture<ErreurPaiementComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ErreurPaiementComponent]
    });
    fixture = TestBed.createComponent(ErreurPaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
