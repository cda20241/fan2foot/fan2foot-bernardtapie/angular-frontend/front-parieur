export interface Utilisateur {
    id:number;
    role: String;
    nom: String;
    prenom: String;
    email: String;
    mdp: String;
    date_naissance: Date;
    avatar: Number;
    favoris : favoris[]
}

export interface UtilisateurIdRole{
    id:number;
    role: String;
    nom: String;
    prenom: String;
    email: String;
    mdp: String;
    date_naissance: Date;
    avatar: Number;
    favoris : favoris[]
}


export interface favoris {
    id:number
    idJoueur:number
}