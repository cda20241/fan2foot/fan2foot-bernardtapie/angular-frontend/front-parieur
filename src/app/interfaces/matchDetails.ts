import {JoueurDTO} from "./joueur";

export interface MatchDetails {
    id: number;
    date: string;
    heure: string;
    scoreDomicile: number;
    scoreExterieur: number;
    butsDomicile: JoueurDTO[];
    butsExterieur:JoueurDTO[];
    passesDomicile: JoueurDTO[];
    passesExterieur:JoueurDTO[];
    eqDomicile: EquipeDetails;
    eqExterieur: EquipeDetails;
  }

  export interface EquipeDetails {
    id: number;
    nomLong: string;
    nomCourt: string;
    championnat: string;
    archive: boolean;
    joueurs: JoueursDetails[];
  }

    export interface JoueursDetails {
      id: number;
      nomCourt: string;
    }
