import { EquipeJoueurs } from "./equipe";
import { Joueur } from "./joueur";
import {Pari} from "./pari";


export interface Match {
  id: number;
  date: string;
  heure: string;
  scoreDomicile: number;
  scoreExterieur: number;
  eqDomicile:EquipeJoueurs;
  eqExterieur:EquipeJoueurs;
  butsDomicile: Joueur[];
  butsExterieur: Joueur[];
  passesDomicile : Joueur[];
  passesExterieur : Joueur[];
  pari: Pari
}

export interface MatchDTO{
  id: number;
  date: string;
  heure: string;
  scoreDomicile: number;
  scoreExterieur: number;
  eqDomicile:EquipeJoueurs;
  eqExterieur:EquipeJoueurs;
}
export interface MatchsParDate {
  date: string;
  matches: Match[];
}
