import { Joueur } from "./joueur";
import { MatchDTO } from "./match";

export interface Equipe{
  id:number;
  nomLong: string;
  nomCourt: string;
  championnat: string;
  archive: boolean;
}

export interface EquipeJoueurs{
    id:number;
    nomLong: string;
    nomCourt: string;
    championnat: string;
    archive: boolean;
    joueurs: Joueur[];
    rencontres: MatchDTO[];

}


