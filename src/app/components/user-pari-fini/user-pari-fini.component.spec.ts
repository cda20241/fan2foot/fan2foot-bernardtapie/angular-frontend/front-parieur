import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPariFiniComponent } from './user-pari-fini.component';

describe('UserPariFiniComponent', () => {
  let component: UserPariFiniComponent;
  let fixture: ComponentFixture<UserPariFiniComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserPariFiniComponent]
    });
    fixture = TestBed.createComponent(UserPariFiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
