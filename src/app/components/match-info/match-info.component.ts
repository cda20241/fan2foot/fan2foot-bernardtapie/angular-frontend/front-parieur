import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {MatchDetails} from 'src/app/interfaces/matchDetails'

@Component({
  selector: 'app-match-info',
  templateUrl: './match-info.component.html',
  styleUrls: ['./match-info.component.scss'],
  providers: [DatePipe]
})
export class MatchInfoComponent implements OnInit {
  @Input() matchDetails$: Observable<MatchDetails> | any;
  match!:MatchDetails;
  @Input() scoreDomicile!:number;
  @Input() scoreExterieur!:number;
  @Input() date!: string;
  @Input() heure!: string;

  jour?:string;
  jourMois?:string;
  heureMinutes?:string;
  statutMatch?:string;

  constructor(private datePipe:DatePipe){}

  ngOnInit(): void {
    if (this.matchDetails$) {
      this.matchDetails$.subscribe({
        next: (details: MatchDetails) => {
          this.match = details;
          this.scoreExterieur = details.scoreExterieur;
          this.scoreDomicile = details.scoreDomicile;
          this.jour=this.extraireNomJour(this.match.date)!;
          this.jourMois=this.extraireJourEtMois(this.match.date)!;
          this.heureMinutes=this.extraireHeureEtMinute(this.match.heure);
          this.statutMatch= this.calculerStatutMatch(this.match.date, this.match.heure);
        },
        error: (error: any) => {
          console.error('Erreur lors de la récupération des détails du match :', error);
        }
      });
    } else {
      console.error('Aucun détail de match fourni.');
    }

  }

  // méthode pour extraire le nom du jour
  extraireNomJour(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'EEEE', 'fr');
  }

  // méthode pour extraire le jour et le mois
  extraireJourEtMois(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'dd MMMM', 'fr');
  }

  // méthode pour extraire l'heure sous format 00H00
  extraireHeureEtMinute(timeString: string): string {
    const [heure, minute] = timeString.split(':');
    const heureFormat = heure + 'H';
    const minuteFormat = minute;
    return `${heureFormat}${minuteFormat}`;
  }

  // méthode pour calculer le statut du match en fonction de l'heure et la date
  calculerStatutMatch(dateString: string, timeString: string): string {
    const dateMatch = new Date(dateString);
    const [heure, minute] = timeString.split(':');
    dateMatch.setHours(parseInt(heure, 10), parseInt(minute, 10), 0, 0);
    console.log(dateMatch)
    const maintenant = new Date();
    console.log(maintenant)
    const differenceMinutes = Math.round((dateMatch.getTime() - maintenant.getTime()) / (1000 * 60));
    console.log(differenceMinutes)
    if (differenceMinutes > 90) {
      return 'À venir';
    } else if (differenceMinutes <= 0 && differenceMinutes >= -90) {
      return 'En cours';
    } else {
      return 'Terminé';
    }
  }
}
