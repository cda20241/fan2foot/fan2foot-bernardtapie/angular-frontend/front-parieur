import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParrainageComponent } from './parrainage.component';

describe('ParrainageComponent', () => {
  let component: ParrainageComponent;
  let fixture: ComponentFixture<ParrainageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParrainageComponent]
    });
    fixture = TestBed.createComponent(ParrainageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
