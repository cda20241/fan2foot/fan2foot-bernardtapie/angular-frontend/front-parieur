import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPariComponent } from './user-pari.component';

describe('UserPariComponent', () => {
  let component: UserPariComponent;
  let fixture: ComponentFixture<UserPariComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserPariComponent]
    });
    fixture = TestBed.createComponent(UserPariComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
