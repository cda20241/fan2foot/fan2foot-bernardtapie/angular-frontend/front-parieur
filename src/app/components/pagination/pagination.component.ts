import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})

export class PaginationComponent {
  @Input() items: any[] = []; // Déclaration de items comme input

  @Input() paginationCurrentPage!: number;
  @Input() paginationItemsPerPage!: number;
  @Input() maxPagesToShow!: number;

  @Output() pageChanged: EventEmitter<number> = new EventEmitter<number>();

  totalItems: number = 0;
  totalPage: number = 0;

  constructor() {}

  ngOnInit(): void {
    this.getAllItems();
    this.getTotalPage();
    this.debugLog();
  }

  debugLog(): void {
    console.log("items", this.items)
    console.log("TotalItems:", this.totalItems)
    console.log("TotalPage:", this.totalPage)
  }

  getAllItems(): number {
    return this.totalItems = this.items.length;
  }

  getTotalPage(): number {
    return this.totalPage = Math.ceil(this.totalItems / this.paginationItemsPerPage);
  }

  // Méthode pour générer la liste des numéros de page
  getPageNumbers(): number[] {
    const maxPagesToShow = this.maxPagesToShow // Nombre maximum de pages à afficher
    const pageNumbers: number[] = [];
    const totalPages = this.totalPage;

    let startPage: number;
    let endPage: number;

    // Si le nombre total de pages est inférieur ou égal à maxPagesToShow,
    // on affiche toutes les pages
    if (totalPages <= maxPagesToShow) {
      startPage = 1;
      endPage = totalPages;
    } else {
      // Sinon, on calcule la plage de pages à afficher
      const maxPagesBeforeCurrentPage = Math.floor(maxPagesToShow / 2);
      const maxPagesAfterCurrentPage = Math.ceil(maxPagesToShow / 2) - 1;

      if (this.paginationCurrentPage <= maxPagesBeforeCurrentPage) {
        // Si la page actuelle est proche du début, on affiche les premières pages
        startPage = 1;
        endPage = maxPagesToShow;
      } else if (this.paginationCurrentPage + maxPagesAfterCurrentPage >= totalPages) {
        // Si la page actuelle est proche de la fin, on affiche les dernières pages
        startPage = totalPages - maxPagesToShow + 1;
        endPage = totalPages;
      } else {
        // Sinon, on affiche les pages autour de la page actuelle
        startPage = this.paginationCurrentPage - maxPagesBeforeCurrentPage;
        endPage = this.paginationCurrentPage + maxPagesAfterCurrentPage;
      }
    }

    for (let i = startPage; i <= endPage; i++) {
      pageNumbers.push(i);
    }

    return pageNumbers;
  }

  setPage(pageNumber: number): void {
    if (pageNumber >= 1 && pageNumber <= this.totalPage) {
      this.paginationCurrentPage = pageNumber;
      this.pageChanged.emit(pageNumber); // Émet l'événement pour informer le composant parent du changement de page
    }
  }

  nextPage(): void {
    if (this.paginationCurrentPage < this.totalPage) {
      this.paginationCurrentPage++;
      this.pageChanged.emit(this.paginationCurrentPage); // Émet l'événement pour informer le composant parent du changement de page
    }
  }

  prevPage(): void {
    if (this.paginationCurrentPage > 1) {
      this.paginationCurrentPage--;
      this.pageChanged.emit(this.paginationCurrentPage);
    }
  }

}

