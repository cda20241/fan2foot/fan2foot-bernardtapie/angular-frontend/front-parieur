import { Component } from '@angular/core';
import {EquipeService} from "../../services/equipe.service";
import {Equipe} from "../../interfaces/equipe";
import {Match} from "../../interfaces/match";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-equipes-liste',
  templateUrl: './equipes-liste.component.html',
  styleUrls: ['./equipes-liste.component.scss']
})
export class EquipesListeComponent {
  equipes: Equipe[] = [];
  equipe!: Equipe;
  // VARIABLE DE PAGINATION
  paginationCurrentPage: number = 1; // Page actuelle
  paginationItemsPerPage: number = 5; // Nombre d'éléments par page
  paginatedItems: Equipe[] = []; // Joueurs paginés pour la vue actuelle
  maxPagesToShow: number = 15; // Nombre maximum de pages à afficher

  constructor(private equipeService:EquipeService,
    private modalService: NgbModal){}

  ngOnInit(): void {
    this.getEquipes();
  }

  // Méthode pour récupérer la liste des équipes
  getEquipes() {
    this.equipeService.getEquipeList().subscribe(
      (equipes) => {
        this.equipes = equipes;
        this.paginateItems()
      },
      (error) => {
        console.error('Erreur lors de la récupération des joueurs:', error);
      });
  }





  //Methodes de PAGINATION
  paginateItems(): void {
    const startIndex = (this.paginationCurrentPage - 1) * this.paginationItemsPerPage;
    const endIndex = startIndex + this.paginationItemsPerPage;
    if (this.equipes) {
      console.log('Start Index:', startIndex);
      console.log('End Index:', endIndex);
      this.paginatedItems = this.equipes.slice(startIndex, endIndex);
    }
  }
  onPageChanged(pageNumber: number): void {
    this.paginationCurrentPage = pageNumber;
    this.paginateItems(); // Rafraîchir les joueurs paginés pour la nouvelle page sélectionnée
  }


  /*---------------- Affichage des logos des equipes des joueurs -----------------*/
  getEquipeLogo(equipe : Equipe): string {
    if (equipe.nomLong === "Glasgow Ranger") {
      return "../assets/images/equipeLogo/glasgowRangers.png";
    }
    else if (equipe.nomLong == "Celtic Glasgow") {
      return "../assets/images/equipeLogo/celtic.png";
    }
    else if (equipe.nomLong == "Heart of Midlothian FC") {
      return "../assets/images/equipeLogo/heart.png";
    }
    else if (equipe.nomLong == "Kilmarnock FC") {
      return "../assets/images/equipeLogo/kilmarnock.png";
    }
    else if (equipe.nomLong == "St Mirren FC") {
      return "../assets/images/equipeLogo/saintMirren.png";
    }
    else if (equipe.nomLong == "Hibernian FC") {
      return "../assets/images/equipeLogo/hibernian.png";
    }
    else if (equipe.nomLong == "Dundee FC") {
      return "../assets/images/equipeLogo/fc-dundee-logo.png";
    }
    else if (equipe.nomLong == "Motherwell FC") {
      return "../assets/images/equipeLogo/motherwell.png";
    }
    else if (equipe.nomLong == "Aberdeen FC") {
      return "../assets/images/equipeLogo/aberdeen.png";
    }
    else if (equipe.nomLong == "St Johnstone FC") {
      return "../assets/images/equipeLogo/stJohnstone.png";
    }
    else if (equipe.nomLong == "Ross County") {
      return "../assets/images/equipeLogo/rossCounty.png";
    }
    else if (equipe.nomLong == "Livingston FC") {
      return "../assets/images/equipeLogo/livingstone.png";
    }
    return "../assets/images/equipeLogo/Scottish_Premiership.png";
  }

}
