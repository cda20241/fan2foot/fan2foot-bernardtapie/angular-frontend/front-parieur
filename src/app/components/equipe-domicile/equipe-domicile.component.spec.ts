import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipeDomicileComponent } from './equipe-domicile.component';

describe('EquipeDomicileComponent', () => {
  let component: EquipeDomicileComponent;
  let fixture: ComponentFixture<EquipeDomicileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipeDomicileComponent]
    });
    fixture = TestBed.createComponent(EquipeDomicileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
