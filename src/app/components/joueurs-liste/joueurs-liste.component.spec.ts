import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JoueursListeComponent } from './joueurs-liste.component';

describe('JoueursListeComponent', () => {
  let component: JoueursListeComponent;
  let fixture: ComponentFixture<JoueursListeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JoueursListeComponent]
    });
    fixture = TestBed.createComponent(JoueursListeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
