import { Component, OnInit } from "@angular/core";
import { Match } from "src/app/interfaces/match";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import { RencontreService } from "src/app/services/rencontre.service";


@Component({
  selector: 'app-matchs-liste',
  templateUrl: './match-liste.component.html',
  styleUrls: ['./match-liste.component.scss']
})
export class MatchListeComponent implements OnInit {
  matchs: Match[] = [];

    // VARIABLE DE PAGINATION
    paginationCurrentPage: number = 1; // Page actuelle
    paginationItemsPerPage: number = 4; // Nombre d'éléments par page
    paginatedItems: Match[] = []; // Joueurs paginés pour la vue actuelle
    maxPagesToShow: number = 15; // Nombre maximum de pages à afficher

  constructor(private rencontreService : RencontreService, private modalService: NgbModal){}

  ngOnInit(): void {
    this.getMatchs();
  }

  // Méthode pour récupérer la liste des matchs
  getMatchs() {
    this.rencontreService.getListeMatchs().subscribe(
      (matchs) => {
        this.matchs = matchs;
        this.paginateItems()
      },
      (error) => {
        console.error("Erreur lors de la récupération des joueurs:", error);
      }
    );
  }

  //Methodes de PAGINATION
    paginateItems(): void {
      const startIndex = (this.paginationCurrentPage - 1) * this.paginationItemsPerPage;
      const endIndex = startIndex + this.paginationItemsPerPage;
      if (this.matchs) {
        console.log('Start Index:', startIndex);
        console.log('End Index:', endIndex);
        this.paginatedItems = this.matchs.slice(startIndex, endIndex);
      }
    }
    onPageChanged(pageNumber: number): void {
      this.paginationCurrentPage = pageNumber;
      this.paginateItems(); // Rafraîchir les joueurs paginés pour la nouvelle page sélectionnée
    }
 
    

    }


