import {Component, Input, OnInit} from '@angular/core';
import {Match} from "../../interfaces/match";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-pari',
  templateUrl: './pari.component.html',
  styleUrls: ['./pari.component.scss'],
  providers: [DatePipe]
})
export class PariComponent implements OnInit{
  @Input() match!:Match
  jour?:string;
  heureMinutes?:string;
  statutMatch?:string;

  constructor(private datePipe:DatePipe) {}
  ngOnInit() {
    this.jour=this.extraireNomJour(this.match.date)!;
    this.heureMinutes=this.extraireHeureEtMinute(this.match.heure);
    this.calculerStatutMatch(this.match.date, this.match.heure)
  }

  // méthode pour extraire le nom du jour
  extraireNomJour(dateString: string) {
    const date = new Date(dateString);
    return this.datePipe.transform(date, 'EEEE', 'fr');
  }


  // méthode pour extraire l'heure sous format 00H00
  extraireHeureEtMinute(timeString: string): string {
    const [heure, minute] = timeString.split(':');
    const heureFormat = heure + 'H';
    const minuteFormat = minute;
    return `${heureFormat}${minuteFormat}`;
  }

  // méthode pour calculer le statut du match en fonction de l'heure et la date
  calculerStatutMatch(dateString: string, timeString: string): string {
    const dateMatch = new Date(dateString);
    const [heure, minute] = timeString.split(':');
    dateMatch.setHours(parseInt(heure, 10), parseInt(minute, 10), 0, 0);
    console.log(dateMatch)
    const maintenant = new Date();
    console.log(maintenant)
    const differenceMinutes = Math.round((dateMatch.getTime() - maintenant.getTime()) / (1000 * 60));
    console.log(differenceMinutes)
    if (differenceMinutes > 90) {
      return this.statutMatch = 'À venir';
    } else if (differenceMinutes <= 0 && differenceMinutes >= -90) {
      return this.statutMatch = 'En cours';
    } else {
      return this.statutMatch = 'Terminé';
    }
  }


}
