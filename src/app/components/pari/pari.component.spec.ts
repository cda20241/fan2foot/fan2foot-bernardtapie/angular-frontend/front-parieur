import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PariComponent } from './pari.component';

describe('PariComponent', () => {
  let component: PariComponent;
  let fixture: ComponentFixture<PariComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PariComponent]
    });
    fixture = TestBed.createComponent(PariComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
