import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationsMatchComponent } from './informations-match.component';

describe('InformationsMatchComponent', () => {
  let component: InformationsMatchComponent;
  let fixture: ComponentFixture<InformationsMatchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InformationsMatchComponent]
    });
    fixture = TestBed.createComponent(InformationsMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
